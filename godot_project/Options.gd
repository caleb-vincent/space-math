extends Node

enum Difficulty {
	EASY,
	NORMAL,
	HARD
}

var selectedDifficulty = Difficulty.NORMAL setget _setDifficulty
var savedPassword : String setget _setPassword

const SAVE_FILE = "user://options.save"


func _init() -> void:
	var f = File.new()
	if f.file_exists(SAVE_FILE):
		f.open(SAVE_FILE, File.READ)
		savedPassword = f.get_var()
		selectedDifficulty = f.get_var()


func _setDifficulty(d : int) -> void:
	selectedDifficulty = d
	_save()


func _save() -> void:
	var f = File.new()
	f.open(SAVE_FILE, File.WRITE)
	f.store_var(savedPassword)
	f.close()


func _setPassword(p : String) -> void:
	savedPassword = p
	_save()
